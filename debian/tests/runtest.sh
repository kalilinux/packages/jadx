#!/bin/sh

set -e

echo "Test jadx -h"
jadx -h

#echo "Test jadx-gui -h"
#jadx-gui

echo "Test decompiler jadx"
WORKDIR=$(mktemp -d)
trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM

(cd debian/tests/HelloWorld/ && ./generate-jar.sh)
jadx -d $WORKDIR $PWD/debian/tests/HelloWorld/HelloWorld.jar
if ! [ -d $WORKDIR/resources ]; then
    echo "jadx fails"
fi

rm -rf $WORKDIR
